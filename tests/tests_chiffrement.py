import unittest
from app import chiffrement

class TestChiffrementFunction(unittest.TestCase):
    
    def test_chiffrage(self):
        """
        Tests unitaires permettant de tester un chiffrement
        non-circulaire.
        """
        phrase1 = "J'aime la salade"
        phrase2 = "Vive Windows :^)"
        self.assertEqual(chiffrement(phrase1, 2, False),'L)ckog"nc"ucncfg')
        self.assertEqual(chiffrement(phrase2, 4, False),"Zmzi$[mrhs{w$>b-")

        """
        Test unitaire permettant de tester un chiffrement
        circulaire.
        """
        phrase3 = "Le gras c'est la vie"
        self.assertEqual(chiffrement(phrase3, 10, True), "Vo qbkc m'ocd vk fso")

    def test_dechiffrage(self):
        """
        Test unitaire permettant de tester un déchiffrement
        non-circulaire.
        """
        code1 = "F*hvw#sdv#idx{"
        self.assertEqual(chiffrement(code1, -3, False),"C'est pas faux")

        """
        Test unitaire permettant de tester un déchiffrement
        circulaire.
        """
        code2 = "T'knybo vk wédryny"
        self.assertEqual(chiffrement(code2, -10, True),"J'adore la méthodo")